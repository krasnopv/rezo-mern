const express = require("express")
const {requireSignIn} = require("../controllers/auth")
const {addPost, getAllPosts, userPosts, getPostById, isOwner, deletePost, likePost, unlikePost, addComment, deleteComment} = require("../controllers/post")
const { getUserById } = require("../controllers/user")

const router = express.Router()


router.get("/api/posts/:userId", requireSignIn, getAllPosts)
//show all posts by user
router.get("/api/posts/by/:userId", requireSignIn, userPosts)
//delete a post
router.delete("/api/post/:postId", requireSignIn, isOwner, deletePost)
//create a post
router.post("/api/posts/:userId", requireSignIn, addPost)

//like a post
router.put("/api/post/like", requireSignIn, likePost)
//unlike a post
router.put("/api/post/unlike", requireSignIn, unlikePost)

//add comment
router.put("/api/post/comment", requireSignIn, addComment)
//delete comment
router.put("/api/post/uncomment", requireSignIn, deleteComment)

//BINDS
router.param("userId", getUserById)
router.param("postId", getPostById)


module.exports = router
