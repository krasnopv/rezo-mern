const express = require("express")
const {createUser, getUserById, getUser, getAllUsers, updateUser, deleteUser, getUserPhoto, addFollower, addFollowing, removeFollowing, removeFollower} = require("../controllers/user")
const {signIn, requireSignIn, hasAuthorization, signOut} = require("../controllers/auth")

const router = express.Router()

//Users
router.post("/api/users/", createUser)
router.get("/api/user/:userId", getUser)
router.get("/api/users", requireSignIn,  getAllUsers)
router.get("/api/user/photo/:userId",  getUserPhoto)
router.put("/api/user/:userId", requireSignIn, hasAuthorization, updateUser)
router.delete("/api/user/:userId", requireSignIn, hasAuthorization, deleteUser)

//Follow system
router.route("/api/user/add/follow").put(requireSignIn,  addFollowing, addFollower)
router.route("/api/user/remove/follow").put(requireSignIn,  removeFollowing, removeFollower)




//Authenticate
router.post("/api/auth/sign-in", signIn)
router.get("/api/auth/sign-out", signOut)


router.param("userId", getUserById)

module.exports = router