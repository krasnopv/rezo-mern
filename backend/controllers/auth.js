const User = require("../models/user")
const jwt = require("jsonwebtoken")
const expressJwt = require("express-jwt")

//retrieve .env variables
require("dotenv").config()

//SignIn feature
const signIn = (req, res) => {
    User.findOne({email: req.body.email}, (error,user)=>{
        if (error || !user){
            return res.json({error: "User not found"})
        }
        user.comparePassword(req.body.password, (error, isMatch)=>{
            if (error) return res.json({error})
            if(!isMatch) return res.json({error: "Email and password doesn't work !"})
            const token = jwt.sign({_id: user._id}, process.env.JWT)
            res.cookie("t", token, {
                expire: new Date() + 4321
            })
            user.hashed_password = undefined
            user.salt = undefined
            return res.json({
                token,
                user
            })
        })
    })
}

//SignOut feature
const signOut = (req, res)=> {
    res.clearCookie("t")
    res.json({message: "Disconnected"})
}

//Middleware sign-in
const requireSignIn = expressJwt({
    secret: process.env.JWT,
    userProperty: "auth",
    algorithms: ["HS256"],
})

const hasAuthorization = (req, res,  next)=>{
    const authorized = req.profile && req.auth && req.profile._id == req.auth._id
    if (!authorized) {
        return res.json({
            error: "You are not authorize"
        })
    }
    next()
}

module.exports =  {
    signIn,
    signOut,
    hasAuthorization,
    requireSignIn
}