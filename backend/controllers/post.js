const Post = require("../models/post")

//retrieve all posts
const getAllPosts = (req,res) => {
    let following = req.profile.following
    following.push(req.profile._id)
    Post.find({postedBy: {$in: req.profile.following}})
        .populate("comments", "text created")
        .populate("comments.postedBy", "_id name")
        .populate("postedBy", "_id name")
        .sort("-createdAt")
        .exec((err, posts)=>{
            if (err) {
                res.json({
                    error: err
                })
            }
            res.json(posts)
        })
}

//retrieve a post by id
const getPostById = (req,res,next,id) => {
    Post.findById(id)
        .populate("comments", "text created")
        .populate("comments.postedBy", "_id name")
        .populate("postedBy", "_id name")
        .exec((err, post)=>{
            if (err) {
                res.json({
                    error: err
                })
            }
            req.post = post
            //console.log(req.post);
            next()
        })
}

//retrieve users posts
const userPosts = (req,res) => {
    Post.find({postedBy: req.profile._id})
        .populate("comments", "text created")
        .populate("comments.postedBy", "_id name")
        .populate("postedBy", "_id name")
        .sort("-createdAt")
        .exec((err, posts)=>{
            if (err) {
                res.json({
                    error: err
                })
            }
            res.json(posts)
        })
}

//add a post
const addPost = (req, res) => {
    const {text} = req.body
    let post = new Post({text, postedBy: req.profile._id})
    post.save((err, data)=> {
        if (err) {
            res.json({
                error: err
            })
        }
        res.json(data)
    })
}

//Test if i'm the owner of the post
const isOwner = (req,res,next)=>{
    let isMine = req.post && req.auth && req.post.postedBy._id == req.auth._id
    if (!isMine) {
        return res.json({error: "Not authorized"})
    }
    next();
}

//delete a post
const deletePost = (req,res)=>{
    //retrieve the post to delete
    let postToDelete = req.post
    postToDelete.remove((error, deletedPost)=>{
        if (error) {
            return res.json({
                error
            })
        }
        res.json(deletedPost)
    })
}

const likePost = (req,res)=>{
    Post.findByIdAndUpdate(
        req.body.postId,
        {$push : {likes: req.body.userId}},
        {new: true})
        .exec((err, result)=>{
            if (err) {
                res.json({error: err})
            }
            res.json(result)
        })
}

const unlikePost = (req,res)=>{
    Post.findByIdAndUpdate(
        req.body.postId,
        {$pull : {likes: req.body.userId}},
        {new: true})
        .exec((err, result)=>{
            if (err) {
                res.json({error: err})
            }
            res.json(result)
        })
}

const addComment = (req,res)=>{
    let comment = {text: req.body.text}
    comment.postedBy = req.body.userId
    Post.findByIdAndUpdate(
        req.body.postId,
        {$push : {comments: comment}},
        {new: true})
        .exec((err, result)=>{
            if (err) {
                return res.json({error: err})
            }
            res.json(result)
        })
}

const deleteComment = (req,res)=>{
    let comment = req.body.comment
    Post.findByIdAndUpdate(
        req.body.postId,
        {$pull : {comments: comment}},
        {new: true})
        .exec((err, result)=>{
            if (err) {
                return res.json({error: err})
            }
            res.json(result)
        })
}

module.exports = {
    getAllPosts,
    addPost,
    userPosts,
    getPostById,
    isOwner,
    deletePost,
    likePost,
    unlikePost,
    addComment,
    deleteComment
}