import React, { useState } from 'react'
import {useDispatch} from "react-redux"
import {isLogged} from "../helpers/auth"
import { addComment } from '../redux/actions/postActions'
import Comment from '../components/Comment'

export default function CommentsList({postId, comments}) {

   const [text, setText] = useState("")

    const dispatch = useDispatch()
    const jwt = isLogged()


    return (
        <React.Fragment>
        <div className="level-left">
            <figure className="image is-48x48 mr-3">
			    <img
				src={`http://localhost:8888/api/user/photo/${jwt.user._id}?${new Date().getTime()}`}
				alt={`profile of ${jwt && jwt.user.name}`}
				/>
			</figure>
            <div style={{width: "100%"}} className="field">
          <label className="label">Comment</label>
          <div className="control ">
          <textarea
            onChange={(event)=>setText(event.target.value)}
            onKeyDown={(event) => {
                if(event.key) {
                    if (event.keyCode === 13 && event.target.value) {
                        dispatch(addComment(jwt.token, jwt.user._id, postId, text))
                        setText("")
                    }
                }
            }}
            value={text}
            name="text"
            className="textarea is-primary"
            placeholder="Type your comment here..."
            required
            />
          </div>
        </div>

        </div>        {
            comments.map((item, i)=> (
                <Comment comment={item} key={i} postId={postId} />
            ))
        }</React.Fragment>
    )
}
