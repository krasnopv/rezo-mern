import React, { useEffect, useState } from 'react'
import Post from "./Post"

export default function PostsList({posts}) {
    const [data, setData] = useState([])

    useEffect(() => {
            setData(posts)
    }, [posts])

    return (
        <div>
            {data && data.map((item, i) => {
                return <Post key={i} post={item} />
            })}
        </div>
    )
}
