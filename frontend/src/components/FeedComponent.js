
import React, { useEffect } from 'react'
import { useDispatch, connect } from 'react-redux'
import { useParams } from 'react-router-dom'
import { isLogged } from '../helpers/auth'
import { getUserPosts } from '../redux/actions/postActions'
import PostsList from './PostsList'

function FeedComponent({userPosts}) {

    const dispatch = useDispatch()
    const jwt = isLogged()
    const {userId} = useParams()

    useEffect(() => {
      
            const loadUserPosts = () => {
                dispatch(getUserPosts(jwt.token,userId))
            }
            loadUserPosts()
        
 
    }, [dispatch, jwt.token, userId])

    return (
        <div>
            <PostsList posts={userPosts}/>
        </div>
    )
}

const mapStateToProps = ({
    post: {
      userPosts
    }
  }) => ({
    userPosts
  })

  export default connect(mapStateToProps,null)(FeedComponent)