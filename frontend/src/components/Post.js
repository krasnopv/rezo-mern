import { faHeart, faReply, faTrash } from '@fortawesome/free-solid-svg-icons'
import heart from "../heart.svg"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { isLogged } from '../helpers/auth'
import {useDispatch} from "react-redux"
import { deletePost, likePost, unlikePost } from '../redux/actions/postActions'
import CommentsList from './CommentsList'



export default function Post({post}) {

    //STATES
    const [likes, setLikes] = useState([])
    const [like, setLike] = useState(false)
    const [comments, setComments] = useState([])


    const date = new Date(post.createdAt)
    const jwt = isLogged()
    const dispatch = useDispatch()
    const postedBy = post.postedBy._id || post.postedBy


    useEffect(() => {
      const checkLike = (likes) => {
        let match = likes.indexOf(jwt.user._id) !== -1
        setLike(match);
      }
          setLikes(post && post.likes)
          setComments(post && post.comments)
          checkLike(post && post.likes)
    }, [post.likes, post.comments, post, jwt.user._id])



    return (
        <div style={{width:"60%", margin: "0 auto"}} className="box">
  <article className="media">
    <div className="media-left">
      <figure className="image is-64x64">
        <img
        src={`http://localhost:8888/api/user/photo/${postedBy}?${new Date().getTime()}`}
		alt={`profile of ${post && post.postedBy.name}`}/>
      </figure>
    </div>
    <div className="media-content">
      <div className="content">
        <p>
          <strong>{post.postedBy.name || jwt.user.name}</strong> <small>{date.toLocaleDateString()}</small> &nbsp;
          {
            postedBy === jwt.user._id && <button onClick={() => dispatch(deletePost(jwt.token, post && post._id))} style={{float: "right"}} className="button is-primary"><FontAwesomeIcon icon={faTrash} className="has-text-light"/></button>
          }
          <br/>
          {post.text}
        </p>
      </div>
      <nav className="level is-mobile">
        <div className="level-left">
          <Link className="level-item" aria-label="reply">
            <span className="icon is-small has-text-primary mr-4">
              {" "}
              {comments.length}{" "}
              <FontAwesomeIcon className="has-text-primary" aria-hidden="true" icon={faReply} />
            </span>
          </Link>
          {
            like ?           <Link className="level-item" aria-label="like">
            <span onClick={()=> dispatch(unlikePost(jwt.token, jwt.user._id, post._id))} className="icon is-small has-text-primary">
              {" "}
              {likes.length}{" "}
              <FontAwesomeIcon icon={faHeart} />
            </span>
          </Link>
          :
          <Link className="level-item" aria-label="like">
          <span onClick={()=> dispatch(likePost(jwt.token, jwt.user._id, post._id))} className="icon is-small has-text-primary">
            {" "}
            {likes.length}{" "}
            <img src={heart} alt="heart"/>
          </span>
        </Link>
          }
        </div>
      </nav>
      <CommentsList postId={post._id} comments={comments && comments}/>
    </div>
  </article>
</div>
    )
}
