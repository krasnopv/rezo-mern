import React, { useState } from 'react'
import {isLogged} from "../helpers/auth"
import {useDispatch} from "react-redux"
import { addPost } from '../redux/actions/postActions'

export default function AddPost() {
    //STATES
    const [post, setPost] = useState({text: ""})

    const jwt = isLogged()
    const dispatch = useDispatch()

    const handleInputChange = (event)=> {
          setPost({...post,[event.target.name]: event.target.value})
      }

      const handleFormSubmit = (event) => {
        event.preventDefault()
        //console.log(user);
        dispatch(addPost(jwt.token, jwt.user._id, post))
        setPost({...post, text: ""})
    }

    return (
        <React.Fragment>
            <article className="message is-primary">
            <div className="message-header">
              <p>Publish</p>
            </div>
        <div className="message-body">
        <form onSubmit={handleFormSubmit}>
        <div className="columns is-tablet">
      <div className="column is-one-quarter "></div>
      <div className="column custom-color">
        <div className="field">
          <label className="label">Message</label>
          <div className="control ">
          <textarea
            onChange={(event)=>handleInputChange(event)}
            value={post.text}
            name="text"
            className="textarea is-primary"
            placeholder="Type your message here..."
            required
            />
          </div>
        </div>
        <div className="field is-grouped pt-5">
          <div className="control">
            <button className="button submit is-primary">Send</button>
          </div>
        </div>
      </div>
      <div className="column is-one-quarter"></div>
    </div>
    </form>
  </div>
</article>
    </React.Fragment>
    )
}
