import { faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { Link, useHistory } from 'react-router-dom'

export default function FollowingComponent({following}) {
    const history = useHistory()

    return (
        <div>
            { following.length > 0 ?
                 following.map((user, index) => (
                    <div
                    style={{cursor: "pointer"}}
                    key={user._id}
                    onClick={()=> history.push(`/user/${user._id}`)}
                    >
                        <div className="media mb-1">
							<div class="media-left">
								<figure className="image is-48x48">
									<img
										src={`http://localhost:8888/api/user/photo/${user._id}`}
										alt={`profile of ${user.name}`}
									/>
								</figure>
							</div>
							<div className="media-content">
								<p className="title is-4" key={index}>
									{user.name}
								</p>
								<p className="subtitle is-6">@{user.name}</p>
							</div>
                            <Link to="#" className="card-header-icon" aria-label="more options">
      <span className="icon">
      <FontAwesomeIcon className="has-text-primary" aria-hidden="true" icon={faAngleDoubleRight} />
      </span>
    </Link>
						</div>

                    </div>
                ))
                :
                <div class="notification is-primary">
                No Following
                </div>
            }
        </div>
    )
}