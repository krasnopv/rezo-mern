import { faTrash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { useDispatch } from 'react-redux'
import { isLogged } from '../helpers/auth'
import { deleteComment } from '../redux/actions/postActions'

export default function Comment({comment, postId}) {
    const date = new Date(comment.created)
    const dispatch = useDispatch()
    const jwt = isLogged()
    const postedBy = comment.postedBy._id || comment.postedBy

    return (
            <div style={{margin: "0 auto"}} className="box">
  <article className="media">
    <div className="media-left">
      <figure className="image is-64x64">
        <img
        src={`http://localhost:8888/api/user/photo/${postedBy}?${new Date().getTime()}`}
		alt={`profile of ${comment && comment.postedBy.name}`}/>
      </figure>
    </div>
    <div className="media-content">
      <div className="content">
        <p>
          <strong>{comment.postedBy.name}</strong> <small>{date.toLocaleDateString()} - {date.toLocaleTimeString()}</small> &nbsp;
          {
            postedBy === jwt.user._id && <button onClick={() => dispatch(deleteComment(jwt.token,jwt.user._id,postId,comment))} style={{float: "right"}} className="button is-primary"><FontAwesomeIcon icon={faTrash} className="has-text-light"/></button>
          }
          <br/>
          {comment.text}
        </p>
      </div>
        </div>
        </article>
        </div>
    )
}
