import React, { Fragment, useEffect, useState } from 'react'
import { Link, useHistory, useParams } from 'react-router-dom'
import { checkAuth, isLogged, logout } from '../helpers/auth'
import { deleteUser, getUser } from '../redux/actions/userActions'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons'
import FollowButton from '../components/FollowButton'
import Tabs from '../components/Tabs'
import {useDispatch, connect} from "react-redux"

function Profile({userSuccess, userError}) {

    //use params
    const {userId} = useParams()

    //STATES
    const [error, setError] = useState("")
    const [following, setFollowing] = useState(false)
    const [user,setUser] = useState(null)
    const [loading, setLoading] = useState(true)

    const history = useHistory()
    const dispatch = useDispatch()
    const jwt = isLogged()
    const date = user && user.createdAt ? new Date(user.createdAt) : null
    
    useEffect(() => {
        async function getProfile(){
            const userData = await getUser(userId, jwt && jwt.token)
            if (userData.error){
                setError(userData.error)
            } else {
                setUser(userData.data)
                setFollowing(checkFollow(userData.data))
            }
        }
        const checkFollow = (user) => {
            const match = user.followers.find((follower) => {
                return follower._id === jwt.user._id
            })
            return match
        }
        if (loading){
            getProfile()
        }
        return ()=> {
            setLoading(false)
        }
    }, [jwt, loading, userId])

    useEffect(() => {
        if (userError && userError !== null) setError(userError)
        if (userSuccess) {
          logout(()=> {
              history.push("/")
          })
          dispatch({type: "TOGGLE_SUCCESS"})
        }
      }, [dispatch, history, userError, userSuccess])

    const showError = ()=> {
        return error && (
          <div className="notification is-danger">
            <button className="delete"></button>
            {error}
          </div>
        )
      }

      const handleButtonClick = (user) => {
          setUser(user)
          setFollowing(!following)
      }

      if (!jwt) {
        return <div className="container">
        <div className="card">
  <div className="card-content">
    <p style={{textAlign: "center"}} className="subtitle">
    Please <Link to="/login">log in</Link> to view this profile
    </p>
  </div>
</div>
</div>
    }

    return (
        <Fragment>
        <section className="hero is-primary is-medium">
            {error ? showError():
            <>
            <div className="hero-body">
  <div className="card-content">
						<div className="media mb-1">
							<div className="media-left">
								<figure className="image is-48x48">
									<img
										src={`http://localhost:8888/api/user/photo/${userId}?${new Date().getTime()}`}
										alt={`profile of ${user && user.name}`}
									/>
								</figure>
							</div>
							<div className="media-content">
								<p className="title is-4" key="1">
									{user && user.name}
								</p>
								<p className="subtitle is-6">{user && user.email}</p>
							</div>
                            <div className="content">
      {user && user.about}
    </div>
						</div>
			</div>
  </div>
  <div className="columns is-variable is-2-mobile is-0-tablet is-3-desktop is-8-widescreen is-1-fullhd">
  <div className="column">
  <span className="tag is-light has-text-weight-bold" style={{margin: "0 0 0.3em 0.3em"}}>Member since: {date && date.toLocaleDateString()}</span>
  </div>
  <div className="column">
  </div>
  <div className="column">
  </div>
  <div className="column">
  {
      checkAuth(userId) ?
      <>
        <span style={{margin: "0 0 0.3em 0.3em"}} className="tag is-info">
            <Link to={`/user/edit/${userId}`}>
            <FontAwesomeIcon className="has-text-light" aria-hidden="true" icon={faEdit}></FontAwesomeIcon>
            &nbsp; Edit Profile
            </Link>
        </span>
        <span style={{margin: "0 0 0.3em 0.3em"}} className="tag is-danger">
            <Link to="#" onClick={()=> dispatch(deleteUser(userId,jwt.token))}>
                <FontAwesomeIcon className="has-text-light" aria-hidden="true" icon={faTrash}></FontAwesomeIcon>
                &nbsp; Delete account
            </Link>
        </span>
    </>
    : 
    <FollowButton
        following={following}
        handleButtonClick={handleButtonClick}
        token={jwt && jwt.token}
        followId={user && user._id}
        userId={jwt && jwt.user._id}
    />
  }
  </div>
</div>
  </>
  }
</section>
<Tabs followers={user && user.followers} following={user && user.following}/>
</Fragment>
    )
}

const mapStateToProps = ({
    user: {
      userError,
      userSuccess
    }
  }) => ({
    userError,
    userSuccess
  })

  export default connect(mapStateToProps,null)(Profile)