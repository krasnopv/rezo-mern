import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import {connect, useDispatch} from "react-redux"
import { checkAuth, isLogged } from '../helpers/auth';
import { getUser, updateUser } from '../redux/actions/userActions';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExclamationTriangle, faUpload } from '@fortawesome/free-solid-svg-icons';

function EditProfile({userError, userSuccess}) {
    const [user, setUser] = useState({
        name: "",
        email: "",
        password: "",
        about: "",
        image: ""
    })

    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true)
    const [fileName, setFileName] = useState("No file selected")

    const dispatch = useDispatch()
    const history = useHistory()
    const jwt = isLogged()
    const {userId} = useParams()
    const userData = new FormData()

    useEffect(() => {
        async function getProfile(){
            const userData = await getUser(userId, jwt && jwt.token)
            if (userData.error){
                setError(userData.error)
            } else {
                setUser(userData.data)
            }
        }
        if (loading){
          getProfile()
      }
      return ()=> {
          setLoading(false)
      }
    }, [jwt, loading, userId])

    useEffect(() => {
      if (userError && userError !== null) setError(userError)
      if (userSuccess) {
        history.push(`/user/${user && user._id}`)
        dispatch({type: "TOGGLE_SUCCESS"})
      }
    }, [dispatch, history, user, userError, userSuccess])

    const handleInputChange = (event)=> {
        const value = event.target.name === "image" ? event.target.files[0] : event.target.value
        if (event.target.name === "image") setFileName(event.target.files[0].name);
        setUser({...user,[event.target.name]: value})
    }

    const showError = ()=> {
      return error && (
        <div class="notification is-danger">
          <button className="delete"></button>
          {error}
        </div>
      )
    }


    const handleFormSubmit = (event) => {
        event.preventDefault()
        user.name && userData.append("name", user.name)
        user.email && userData.append("email", user.email)
        user.password && userData.append("password", user.password)
        user.about && userData.append("about", user.about)
        user.image && userData.append("image", user.image)
        dispatch(updateUser(userData, jwt.token, userId))
    }

    if (!checkAuth(userId)) {
        history.push(`/user/${userId}`)
    }

    return (
        <React.Fragment>
            <article className="message is-primary">
            <div className="message-header">
              <p>Update Profile</p>
            </div>
        <div className="message-body">
        <form onSubmit={handleFormSubmit}>
        <div className="columns is-tablet">
      <div className="column is-one-quarter "></div>
      <div className="column custom-color">
      {showError()}
        <figure style={{margin: "0 auto"}} class="image is-128x128">
  <img alt={user && user.name} class="is-rounded" src={`http://localhost:8888/api/user/photo/${userId}?${new Date().getTime()}`}/>
</figure>
<div class="field">
  <div class="file has-name is-centered is-primary">
    <label class="file-label">
      <input
      class="file-input"
      type="file"
      accept="image/*"
      name="image"
      onChange={(event)=> handleInputChange(event)}
      />
      <span class="file-cta">
        <span class="file-icon">
          <FontAwesomeIcon icon={faUpload} />
        </span>
        <span class="file-label">
          Choose a profile image...
        </span>
      </span>
      <span class="file-name">
        {fileName}
    </span>
    </label>
  </div>
</div>
        <div className="mt-6 field">
          <label className="label">Name</label>
          <div className="control ">
            <input
                name="name"
                value={user.name}
                onChange={(event)=> handleInputChange(event)}
                className="input is-primary"
                type="text"
                placeholder="Name"
            />
          </div>
        </div>
        <div className="field">
          <label className="label">Email</label>
          <div className="control ">
            <input
                required
                value={user.email}
                onChange={(event)=> handleInputChange(event)}
                name="email"
                className="input is-primary"
                type="email"
                placeholder="Email"
            />
          </div>
        </div>
        <div className="field">
          <label className="label">Password &nbsp;<FontAwesomeIcon className="icon is-small is-right has-text-primary" icon={faExclamationTriangle} /></label>
          <div className="control ">
            <input
                name="password"
                value={user.password || ""}
                onChange={(event)=>handleInputChange(event)}
                className="input is-primary"
                type="password"
                placeholder="Password"
            />
          </div>
        </div>
        <div className="field">
          <label className="label">Confirm Password &nbsp;<FontAwesomeIcon className="icon is-small is-right has-text-primary" icon={faExclamationTriangle} /></label>
          <div className="control ">
            <input className="input is-primary" type="password" placeholder="Confirm Password"/>
            <span className="icon is-small is-left">
            </span>
          </div>
        </div>
        <div className="field">
          <label className="label">About</label>
          <div className="control ">
            <textarea
            onChange={(event)=>handleInputChange(event)}
            value={user.about} name="about"
            className="textarea is-primary"
            placeholder="About you"
            />
          </div>
        </div>


        <div className="field is-grouped pt-5">
          <div className="control">
            <button className="button is-primary submit">Update</button>
          </div>
        </div>
      </div>
      <div className="column is-one-quarter"></div>
    </div>
    </form>
  </div>
</article>
    </React.Fragment>
    )
}

const mapStateToProps = ({
  user: {
    userError,
    userSuccess
  }
}) => ({
  userError,
  userSuccess
})

export default connect(mapStateToProps,null)(EditProfile)