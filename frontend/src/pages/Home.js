import React, { useEffect, useState } from 'react'
import {useDispatch, connect} from "react-redux"
import {isLogged} from "../helpers/auth"
import { getAllPosts } from '../redux/actions/postActions'
import {Link} from "react-router-dom"
import PostsList from "../components/PostsList"
import AddPost from '../components/AddPost'

 function Home({posts}) {
   const [loading, setLoading] = useState(true)

    const dispatch = useDispatch()
    const jwt = isLogged()

    useEffect(() => {
        if (loading){
          if (jwt) {
            const loadPosts = () => {
                dispatch(getAllPosts(jwt.token, jwt.user._id))
            }
            loadPosts()
        }
      }
      return ()=> {
          setLoading(false)
      }
    }, [dispatch, jwt, loading])

    if (!jwt) {
        return <div className="container">
        <div class="card">
  <div class="card-content">
    <p style={{textAlign: "center"}} class="subtitle">
    Please <Link to="/login">log in</Link> to view messages
    </p>
  </div>
</div>
</div>
    }

    return (
        <div className="container">
            <AddPost />
            <PostsList posts={posts && posts}/>
        </div>
    )
}

const mapStateToProps = ({
    post: {
      posts
    }
  }) => ({
    posts
  })

  export default connect(mapStateToProps,null)(Home)